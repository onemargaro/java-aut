#JAVA BÁSICO

* Objeto: Instancia de una clase

  ```Java
      //TIPO nombre = new TIPO(constructor);
  ```

* Clase: Template para crear objetos **consideración la clase se debe de llamar igual que el archivo**

  ```Java
      class Clase {
          public Clase(){
            // definición
          }
      }
  ```

* Encapsulamiento: quién puede ver los atributos escritos

  * Modificadores de acceso

    ```Java
        public atributo;
        protected atributo;
        private atributo;
    ```

* Herencia: como un padre a un hijo, le puede heredar cosas

  ```Java
      class Padre {
         public apellido;
         public beber(){
          // como bebe el papá
         }
      }
      class Hijo extends Padre {
         // El hijo hereda el apellido y la acción de beber
      }
  ```

* Polimorfismo: Propiedad para comportarse de diferente manera a lo que se hereda ya sea cambiar o agregar atributos o métodos

  ```Java
      class Hijo extends Padre {
          //sabemos que heredó
          public comer;
          public manejar(){
              //definición: como lo va a hacer
          }
      }
  ```

* Clase Completa con atributos y métodos

  ```Java
      class Padre {
          public nombre;
          public apellido;
          public Padre(String nombre, String apellido){
              this.nombre = nombre;
              this.apellido = apellido;
          }
          public beber(){
              System.out.println(this.nombre + " " + this.apellido + " esta bebiendo vodka");
          }
      }
      class Hijo extends Padre {}
  ```
